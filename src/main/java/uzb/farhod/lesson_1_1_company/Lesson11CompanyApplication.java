package uzb.farhod.lesson_1_1_company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson11CompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson11CompanyApplication.class, args);
    }

}
