package uzb.farhod.lesson_1_1_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.payload.AddressDto;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.service.AddressService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/company/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    /**
     * ADDRESS QO'SHADIGAN METOD
     * @return ApiResponse
     * BIZGA AddressDto tipli json OBJECT KELADI
     * @vaildatsiya qilindi
     */
    @PostMapping
    public HttpEntity<?> addAddress(@Valid @RequestBody AddressDto addressDto){
        ApiResponse apiResponse = addressService.addAddress(addressDto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * Barcha addresslarni olish
     * @return  List<Address>
     *
     */
    @GetMapping
    public HttpEntity<List<Address>> getAdressList(){
        List<Address> addressList = addressService.getAdressList();
        return ResponseEntity.ok().body(addressList);
    }

    /**
     * id bo'yicha 1 ta addressni olish
     * @return  Address
     * BIZGA address idsi keladi
     *
     */
    @GetMapping("/{id}")
    public HttpEntity<Address> getAdressById(@PathVariable Integer id){
        Address adressById = addressService.getAdressById(id);
        return ResponseEntity.ok().body(adressById);
    }

    /**
     * Addressni tahrirlash
     * @param addressDto AddressDto
     * @param id Integer
     * @return ApiResponse
     */
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editAddress(@Valid @RequestBody AddressDto addressDto, @PathVariable Integer id){
        ApiResponse apiResponse=addressService.editAddress(id, addressDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    /**
     * Adressni o'chirish
     * @param id Integer
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public  HttpEntity<?> deleteAddress(@PathVariable Integer id){
        ApiResponse apiResponse=addressService.deleteAddress(id);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
