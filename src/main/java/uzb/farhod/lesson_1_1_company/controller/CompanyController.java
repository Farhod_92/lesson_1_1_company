package uzb.farhod.lesson_1_1_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.payload.AddressDto;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.CompanyDto;
import uzb.farhod.lesson_1_1_company.service.AddressService;
import uzb.farhod.lesson_1_1_company.service.CompanyService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/company/company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    /**
     * COMPANY QO'SHADIGAN METOD
     * @return ApiResponse
     * BIZGA CompanyDto tipli json OBJECT KELADI
     *
     */
    @PostMapping
    public HttpEntity<?> addAddress( @RequestBody CompanyDto companyDto){

        ApiResponse apiResponse = companyService.addCompany(companyDto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * Barcha companiyalarni olish
     * @return  List<Company>
     *
     */
    @GetMapping
    public HttpEntity<List<Company>> getAdressList(){
        List<Company> companyList = companyService.getCompanyList();
        return ResponseEntity.ok().body(companyList);
    }

    /**
     * id bo'yicha 1 ta companyni olish
     * @return  Company
     * BIZGA company idsi keladi
     *
     */
    @GetMapping("/{id}")
    public HttpEntity<Company> getCompanyById(@PathVariable Integer id){
        Company company = companyService.getCompanyById(id);
        return ResponseEntity.ok().body(company);
    }

    /**
     * Addressni tahrirlash
     * @param companyDto CompanyDto
     * @param id Integer
     * @return ApiResponse
     */
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editCompany(@RequestBody CompanyDto companyDto, @PathVariable Integer id){
        ApiResponse apiResponse=companyService.editCompny(id, companyDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    /**
     * Companiyni o'chirish
     * @param id Integer
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public  HttpEntity<?> deleteCompany(@PathVariable Integer id){
        ApiResponse apiResponse=companyService.deleteCompany(id);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
    }

}
