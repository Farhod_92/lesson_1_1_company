package uzb.farhod.lesson_1_1_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.entity.Department;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.CompanyDto;
import uzb.farhod.lesson_1_1_company.payload.DepartmentDto;
import uzb.farhod.lesson_1_1_company.service.CompanyService;
import uzb.farhod.lesson_1_1_company.service.DepartmentService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/company/department")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    /**
     * Department QO'SHADIGAN METOD
     * @return ApiResponse
     * BIZGA DepartmentDto tipli json OBJECT KELADI
     * @vaildatsiya qilindi
     */
    @PostMapping
    public HttpEntity<?> addDepartment(@Valid @RequestBody DepartmentDto departmentDto){

        ApiResponse apiResponse = departmentService.addDepartment(departmentDto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * Barcha departmentlarni olish
     * @return  List<Department>
     *
     */
    @GetMapping
    public HttpEntity<List<Department>> getDepartmentList(){
        List<Department> departmentList = departmentService.getDepartmentList();
        return ResponseEntity.ok().body(departmentList);
    }

    /**
     * id bo'yicha 1 ta Department olish
     * @return  Company
     * BIZGA company idsi keladi
     *
     */
    @GetMapping("/{id}")
    public HttpEntity<Department> getDepartmentById(@PathVariable Integer id){
        Department department = departmentService.getDepartmentById(id);
        return ResponseEntity.ok().body(department);
    }

    /**
     * Departmentni tahrirlash
     * @param departmentDto DepartmentDto
     * @param id Integer
     * @return ApiResponse
     */
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editDepartment(@Valid @RequestBody DepartmentDto departmentDto, @PathVariable Integer id){
        ApiResponse apiResponse=departmentService.editDepartment(id, departmentDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    /**
     * Departmentni o'chirish
     * @param id Integer
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public  HttpEntity<?> deleteDepartment(@PathVariable Integer id){
        ApiResponse apiResponse=departmentService.deleteDepartment(id);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
