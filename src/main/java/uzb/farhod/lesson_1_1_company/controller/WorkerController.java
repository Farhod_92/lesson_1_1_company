package uzb.farhod.lesson_1_1_company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson_1_1_company.entity.Department;
import uzb.farhod.lesson_1_1_company.entity.Worker;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.DepartmentDto;
import uzb.farhod.lesson_1_1_company.payload.WorkerDto;
import uzb.farhod.lesson_1_1_company.service.DepartmentService;
import uzb.farhod.lesson_1_1_company.service.WorkerService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/company/worker")
public class WorkerController {
    @Autowired
    WorkerService workerService;

    /**
     * Worker QO'SHADIGAN METOD
     * @return ApiResponse
     * BIZGA DepartmentDto tipli json OBJECT KELADI
     * @vaildatsiya qilindi
     */
    @PostMapping
    public HttpEntity<?> addWorker(@Valid @RequestBody WorkerDto workerDto){

        ApiResponse apiResponse = workerService.addWorker(workerDto);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * Barcha workerlarni olish
     * @return  List<Worker>
     *
     */
    @GetMapping
    public HttpEntity<List<Worker>> getWorkerList(){
        List<Worker> workerList = workerService.getWorkerList();
        return ResponseEntity.ok().body(workerList);
    }

    /**
     * id bo'yicha 1 ta Worker olish
     * @return  Worker
     * BIZGA Worker idsi keladi
     *
     */
    @GetMapping("/{id}")
    public HttpEntity<Worker> getWorkerById(@PathVariable Integer id){
        Worker worker = workerService.getWorkerById(id);
        return ResponseEntity.ok().body(worker);
    }

    /**
     * Worker tahrniirlash
     * @param workerDto WorkerDto
     * @param id Integer
     * @return ApiResponse
     */
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editWorker(@Valid @RequestBody WorkerDto workerDto, @PathVariable Integer id){
        ApiResponse apiResponse=workerService.editWorker(id, workerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    /**
     * Workerni o'chirish
     * @param id Integer
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public  HttpEntity<?> deleteWorker(@PathVariable Integer id){
        ApiResponse apiResponse=workerService.deleteWorker(id);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
