package uzb.farhod.lesson_1_1_company.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    private String phoneNumber;

    @OneToOne
    private Address address;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Department department;
}
