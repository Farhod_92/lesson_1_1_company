package uzb.farhod.lesson_1_1_company.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    @NotNull(message = "ko'cha nomi kiritilishi shart")
    private String street;

    private String homeNumber;
}
