package uzb.farhod.lesson_1_1_company.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson_1_1_company.entity.Address;

import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDto {

    private String corpName;

    private String directorName;

    private Integer addressId;
}
