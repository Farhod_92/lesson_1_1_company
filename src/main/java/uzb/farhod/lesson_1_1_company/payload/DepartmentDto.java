package uzb.farhod.lesson_1_1_company.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson_1_1_company.entity.Company;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDto {

    @NotNull(message = "department nomi kiritilmadi")
    private String name;

    @NotNull(message = "company id kiritilmadi")
    private Integer companyId;
}
