package uzb.farhod.lesson_1_1_company.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.entity.Department;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkerDto {

    @NotNull(message = "ism kiritilishi shart")
    private String name;

    private String phoneNumber;

    private Integer addressId;

    @NotNull(message = "departament id kiritilishi shart")
    private Integer departmentId;
}
