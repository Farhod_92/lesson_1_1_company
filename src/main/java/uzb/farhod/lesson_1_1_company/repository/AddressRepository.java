package uzb.farhod.lesson_1_1_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson_1_1_company.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
