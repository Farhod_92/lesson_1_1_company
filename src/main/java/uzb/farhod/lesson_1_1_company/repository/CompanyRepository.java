package uzb.farhod.lesson_1_1_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson_1_1_company.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    boolean existsByCorpName(String corpName);

    boolean existsByCorpNameAndIdNot(String corpName, Integer id);
}
