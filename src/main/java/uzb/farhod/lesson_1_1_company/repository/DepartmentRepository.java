package uzb.farhod.lesson_1_1_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
