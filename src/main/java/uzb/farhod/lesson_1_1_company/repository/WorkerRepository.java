package uzb.farhod.lesson_1_1_company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson_1_1_company.entity.Department;
import uzb.farhod.lesson_1_1_company.entity.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Integer> {
}
