package uzb.farhod.lesson_1_1_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.payload.AddressDto;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.repository.AddressRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    AddressRepository addressRepository;

    public ApiResponse addAddress(AddressDto addressDto) {
        Address address=new Address();
        address.setHomeNumber(addressDto.getHomeNumber());
        address.setStreet(addressDto.getStreet());
        addressRepository.save(address);
        return new ApiResponse("adress saqlandi", true);
    }


    public  List<Address> getAdressList() {
        List<Address> all = addressRepository.findAll();
        return all;
    }

    public Address getAdressById(Integer id) {
        Optional<Address> addressOptional = addressRepository.findById(id);
        return addressOptional.orElse(null);
    }

    public ApiResponse editAddress(Integer id, AddressDto addressDto) {
        Optional<Address> addressOptional = addressRepository.findById(id);

        if(!addressOptional.isPresent())
            return new ApiResponse("ushbu id li adress topoilmadi",false);

        Address address = addressOptional.get();
        address.setStreet(addressDto.getStreet());
        address.setHomeNumber(addressDto.getHomeNumber());
        addressRepository.save(address);
        return new ApiResponse("address tahrirlandi", true);
    }

    public ApiResponse deleteAddress(Integer id) {
        try{
            addressRepository.deleteById(id);
            return new ApiResponse("adress o'chirildi", true);
        }catch (Exception ex){
            return new ApiResponse("xatolik adress o'chirilmadi", false);
        }
    }
}
