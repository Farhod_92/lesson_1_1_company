package uzb.farhod.lesson_1_1_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.payload.AddressDto;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.CompanyDto;
import uzb.farhod.lesson_1_1_company.repository.AddressRepository;
import uzb.farhod.lesson_1_1_company.repository.CompanyRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    AddressRepository addressRepository;

    public ApiResponse addCompany(CompanyDto companyDto) {
        boolean b = companyRepository.existsByCorpName(companyDto.getCorpName());
        if(b)
            return new ApiResponse("bunday nomli company mavjud", false);

        Optional<Address> addressOptional=addressRepository.findById(companyDto.getAddressId());

        Company company=new Company();
        if(addressOptional.isPresent())
            company.setAddress(addressOptional.get());
        company.setCorpName(companyDto.getCorpName());
        company.setDirectorName(companyDto.getDirectorName());

        companyRepository.save(company);
        return new ApiResponse("company saved", true);
    }

    public  List<Company> getCompanyList() {
        List<Company> all = companyRepository.findAll();
        return all;
    }

    public Company getCompanyById(Integer id) {
        Optional<Company> companyOptional = companyRepository.findById(id);
        return companyOptional.orElse(null);
    }

    public ApiResponse editCompny(Integer id, CompanyDto companyDto) {
        boolean b = companyRepository.existsByCorpNameAndIdNot(companyDto.getCorpName(), id);
        if(b)
            return new ApiResponse("bunday nomli company mavjud", false);

        Optional<Company> companyOptional = companyRepository.findById(id);

        if(!companyOptional.isPresent())
            return new ApiResponse("ushbu id li company topoilmadi",false);

        Optional<Address> addressOptional=addressRepository.findById(companyDto.getAddressId());

        Company company = companyOptional.get();
        company.setDirectorName(companyDto.getDirectorName());
        company.setCorpName(companyDto.getCorpName());
        if(addressOptional.isPresent())
            company.setAddress(addressOptional.get());
        else
            company.setAddress(null);
        companyRepository.save(company);
        return new ApiResponse("company tahrirlandi", true);
    }

    public ApiResponse deleteCompany(Integer id) {
        try{
            companyRepository.deleteById(id);
            return new ApiResponse("company o'chirildi", true);
        }catch (Exception ex){
            return new ApiResponse("xatolik company o'chirilmadi", false);
        }
    }
}
