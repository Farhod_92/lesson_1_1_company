package uzb.farhod.lesson_1_1_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.entity.Department;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.CompanyDto;
import uzb.farhod.lesson_1_1_company.payload.DepartmentDto;
import uzb.farhod.lesson_1_1_company.repository.AddressRepository;
import uzb.farhod.lesson_1_1_company.repository.CompanyRepository;
import uzb.farhod.lesson_1_1_company.repository.DepartmentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    CompanyRepository companyRepository;

    public ApiResponse addDepartment(DepartmentDto departmentDto) {
        Optional<Company> companyOptional = companyRepository.findById(departmentDto.getCompanyId());

        if(companyOptional.isPresent()){
            Department department=new Department();
            department.setName(departmentDto.getName());
            department.setCompany(companyOptional.get());
            departmentRepository.save(department);
            return new ApiResponse("yangi department qo'shildi", true);
        }
        return new ApiResponse("bunaqa idli company topilmadi", false);
    }

    public  List<Department> getDepartmentList() {
        List<Department> all = departmentRepository.findAll();
        return all;
    }

    public Department getDepartmentById(Integer id) {
        Optional<Department> departmentOptional = departmentRepository.findById(id);
        return departmentOptional.orElse(null);
    }

    public ApiResponse editDepartment(Integer id, DepartmentDto departmentDto) {

        Optional<Department> departmentOptional = departmentRepository.findById(id);

        if(!departmentOptional.isPresent())
            return new ApiResponse("ushbu id li department topilmadi",false);

        Optional<Company> companyOptional=companyRepository.findById(departmentDto.getCompanyId());

        if(companyOptional.isPresent()){
            Department department = departmentOptional.get();
            department.setName(departmentDto.getName());
            department.setCompany(companyOptional.get());
            departmentRepository.save(department);
            return new ApiResponse("department tahrirlandi", true);
        }

        return new ApiResponse("dushbu id li company topilmadi", false);
    }

    public ApiResponse deleteDepartment(Integer id) {
        try{
            departmentRepository.deleteById(id);
            return new ApiResponse("department o'chirildi", true);
        }catch (Exception ex){
            return new ApiResponse("xatolik department o'chirilmadi", false);
        }
    }
}
