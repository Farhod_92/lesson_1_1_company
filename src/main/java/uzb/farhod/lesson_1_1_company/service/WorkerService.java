package uzb.farhod.lesson_1_1_company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson_1_1_company.entity.Address;
import uzb.farhod.lesson_1_1_company.entity.Company;
import uzb.farhod.lesson_1_1_company.entity.Department;
import uzb.farhod.lesson_1_1_company.entity.Worker;
import uzb.farhod.lesson_1_1_company.payload.ApiResponse;
import uzb.farhod.lesson_1_1_company.payload.DepartmentDto;
import uzb.farhod.lesson_1_1_company.payload.WorkerDto;
import uzb.farhod.lesson_1_1_company.repository.AddressRepository;
import uzb.farhod.lesson_1_1_company.repository.CompanyRepository;
import uzb.farhod.lesson_1_1_company.repository.DepartmentRepository;
import uzb.farhod.lesson_1_1_company.repository.WorkerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {
    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    AddressRepository addressRepository;

    public ApiResponse addWorker(WorkerDto workerDto) {
        Optional<Department> departmentOptional = departmentRepository.findById(workerDto.getDepartmentId());
        Optional<Address> addressOptional = addressRepository.findById(workerDto.getAddressId());

        if(departmentOptional.isPresent()){
            Worker worker=new Worker();
            worker.setName(workerDto.getName());
            worker.setPhoneNumber(workerDto.getPhoneNumber());
            worker.setDepartment(departmentOptional.get());
            if(addressOptional.isPresent())
                worker.setAddress(addressOptional.get());
            else
                worker.setAddress(null);

            workerRepository.save(worker);
            return new ApiResponse("yangi worker qo'shildi", true);
        }
        return new ApiResponse("bunaqa idli department topilmadi", false);
    }

    public  List<Worker> getWorkerList() {
        List<Worker> all = workerRepository.findAll();
        return all;
    }

    public Worker getWorkerById(Integer id) {
        Optional<Worker> workerOptional = workerRepository.findById(id);
        return workerOptional.orElse(null);
    }

    public ApiResponse editWorker(Integer id, WorkerDto workerDto) {

        Optional<Worker> workerOptional = workerRepository.findById(id);

        if(!workerOptional.isPresent())
            return new ApiResponse("ushbu id li worker topilmadi",false);

        Optional<Department> departmentOptional=departmentRepository.findById(workerDto.getDepartmentId());
        Optional<Address> addressOptional = addressRepository.findById(workerDto.getAddressId());

        if(departmentOptional.isPresent()){
            Worker worker=workerOptional.get();
            worker.setName(workerDto.getName());
            worker.setPhoneNumber(workerDto.getPhoneNumber());
            worker.setDepartment(departmentOptional.get());
            if(addressOptional.isPresent())
                worker.setAddress(addressOptional.get());
            else
                worker.setAddress(null);

            workerRepository.save(worker);
            return new ApiResponse("yangi worker qo'shildi", true);
        }

        return new ApiResponse("ushbu id li department topilmadi", false);
    }

    public ApiResponse deleteWorker(Integer id) {
        try{
            workerRepository.deleteById(id);
            return new ApiResponse("worker o'chirildi", true);
        }catch (Exception ex){
            return new ApiResponse("xatolik worker o'chirilmadi", false);
        }
    }
}
